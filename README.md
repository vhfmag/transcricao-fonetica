# Transcrição fonética com aprendizado de máquina
Esse é o esboço de um ideia de projeto, para fins de documentação e colaboração.

## Introdução
Esse projeto tem como objetivo coletar dados (áudios e suas correspondentes transcrições fonéticas) para uma base aberta de dados, a partir da qual se projetará um programa capaz de realizar transcrição fonética automaticamente. Outro atributos, como entonação, acento e ritmo, seriam estudados num segundo momento, possibilitando novas aplicações como o estudo de línguas tonais.

Possíveis aplicações:

- Em pesquisa etnográfica, fornece ao pesquisador material de estudo sobre línguas orais mais amplo e objetivo e mais facilmente documentado
- Servir como base comum a muitas línguas (senão todas) para outras aplicações que envolvam processamento de fala humana, como transcrição de texto e análise emocional
- Pesquisa e diagnóstico em fonoaudiologia

## Motivação
Tem se tornado cada vez mais corriqueiro o uso de dispositivos e programas que transcrevem e interpretam a voz humana. Exemplos são assistentes pessoais como o [Google Home](https://store.google.com/?srp=/product/google_home) e o [Amazon Echo](https://www.amazon.com/dp/B06XCM9LJ4/), assim como a pesquisa por voz do Google, que é também capaz de, ao comando do usuário, fazer ligações, agendar compromissos e dizer a previsão do tempo. Para tal, se usam programas que convertem trechos de áudio com fala humana diretamente em texto, na língua em que se fala. O último trecho é importante: para diferentes línguas, é necessário usar - e projetar - diferentes programas.

Para fazê-lo, o método mais popular atualmente é o aprendizado de máquina, disciplina em que, em termos gerais, um programa recebe uma _grande quantidade_ de pares de informações (para 'aprender' a traduzir de inglês para português, por exemplo, tradutores automáticos podem receber pares de sentença em inglês e seu equivalente em português) e, um a um, usam uma das informações (a sentença em inglês) para adivinhar a outra (a sentença em português). Aprendendo com os seus erros, programas que usam do método podem chegar a resultados satisfatórios para uma classe de tarefas que antes apenas humanos conseguiam realizar, chegando, por vezes, a superar o desempenho dos humanos (como diagnósticos de doenças mais precisos que os de médicos).

O código do programa torna-se apenas mais uma parte do método, nesse paradigma. Não importa quão bom seja seu desenvolvedor, seus resultados serão apenas tão bons quanto os dados que lhe são fornecidos. Além disso, o desenvolvimento é custoso: o volume de dados é grande e o tempo para processá-lo, também - especialmente pela natureza do método, que precisa de várias iterações de tentativa e erro.

Resulta disso que:

- Apesar da natureza linguística do problema da transcrição de fala (e do grande potencial para a disciplina de suas soluções), seu uso para estudo e pesquisa linguística é inócuo
- O acesso a essa tecnologia está restrito às línguas e dialetos escritos e com suficiente arquivo (já que exige arquivo oral e escrito)
- Todo projeto de programa para transcrição automática precisa, mais uma vez, aprender a transformar fala em texto, com pouco reaproveitamento do que foi anteriormente feito com outras línguas para além da camada de arquiteturas e algoritmos

Por isso, propõe-se uma nova solução, que envolva uma etapa intermediária na transcrição automática de fala humana: se antes se transformava fala em texto, se sugere que se transforme fala em transcrição fonética, para então transformá-la em texto. Para a construção desta, são bem vindos os registros de toda e qualquer língua - preferencial mas não obrigartoriamente acompanhados de suas transcrições fonéticas[^iff-semi-supervised]. Qualquer língua (não tonal, num primeiro momento) cujos fonemas já tiverem sido mapeados pela solução poderia se beneficiar dela, mesmo que a língua em si não conste no corpus utilizado para desenvolvê-la.

## Metodologia técnica

Será necessário um estudo minucioso de literatura relacionada para decisões de arquitetura e algoritmo. A ideia inicial, no entanto, é utilizar uma arquitetura de redes neurais recorrentes (RNN) fazendo uso de unidades Long-short Term Memory (LSTM) num esquema de aprendizado semi-supervisionado, conforme demonstrado possível por pesquisadores em dois artigos citados[^google-semi-rnn-reference] [^arvix-semi-rnn-reference], ambos no domínio de processamento de língua natural (NLP).

Fundamentação das escolhas:

- Redes neurais recorrentes (RNN) estão por trás do estado da arte em processamento de línguas naturais.
- Unidades LSTM é frequentemente empregado em problemas de NLP e reconhecimento de fala e soluciona alguns problemas comuns em outras unidades de redes neurais (NN) dotadas de memória (_exploding and vanishing gradients_, por exemplo)
- O paradigma de aprendizado mais adequado para o problema é o supervisionado, já que o mesmo é de regressão. Porém, tendo em vista a abundância de dados de entrada (fala humana) e a escassez dos de saída (transcrição fonética), o uso de técnicas de aprendizado semi-supervisionado permitiria extrair o máxima de informação da estrutura estatística da fala.

## Coleta de dados

Conforme citado anteriormente, para desenvolver o programa será necessário uma vasta quantidade de dados. Mais especificamente, de pares de registro sonoro de fala humana e seu equivalente em alfabeto fonético. Enquanto pares de fala e texto são relativamente abundantes na internet, por exemplo, o corpus é, nesse caso, difícil de obter. Esse é o maior gargalo do projeto atualmente, e se buscam soluções para tal.

Possíveis soluções:

- Portal colaborativo para doação de dados. Nesse caso, professores poderiam estimular a alimentação de dados dentro de grupos de pesquisa, por exemplo.

[^iff-semi-supervised]: Desde que a proposta de uma arquitetura de aprendizado semi-super supervisionado seja adequada para o problema
[^google-semi-rnn-reference]: https://papers.nips.cc/paper/5949-semi-supervised-sequence-learning.pdf
[^arvix-semi-rnn-reference]: https://arxiv.org/pdf/1602.02373.pdf
